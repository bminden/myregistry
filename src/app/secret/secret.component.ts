import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {SessionStorageService, SessionStorage } from 'angular-web-storage';

@Component({
  selector: 'app-secret',
  templateUrl: './secret.component.html',
  styleUrls: ['./secret.component.css']
})
export class SecretComponent implements OnInit {
  adults: number;
  children: number;
  totalGifts: number;
  RSVP: Observable<any[]>;
  gifts: Observable<any[]>;
  constructor(public firestore: AngularFirestore, public session: SessionStorageService) {

  }

  ngOnInit() {
    this.RSVP = this.firestore.collection('test-RSVP').valueChanges();
    this.gifts = this.firestore.collection('test-gifts').valueChanges();
  }
}
