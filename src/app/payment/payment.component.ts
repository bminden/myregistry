import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import {SessionStorageService, SessionStorage } from 'angular-web-storage';
import {Location} from '@angular/common';

declare var paypal;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  item: any;
  constructor(private router: Router, public session: SessionStorageService, private location: Location) { }

  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;

  ngOnInit(): void {
    this.item = this.session.get("item");
  }

  validateAndMove(){
    if(this.session.get("value") != null && this.session.get("party") != null && this.session.get("email") != null){
      if(this.checkFormat(this.session.get("value"))){
        this.router.navigate(['/confirm']);
      }else{
        alert("Please make sure you entered the number correctly in whole dollars.")
      }
    }else{
      alert("Some information is missing!");
    }
  }

  checkFormat(value: number){
    var numbers = new RegExp(/^[0-9]+$/);
    if(numbers.test(value.toString())){
      return true;
    }
    return false;
  }

  updateValue(value: String) {
    this.session.set("value",value);
    console.log(this.session.get("value"));
  }

  updateParty(party: String){
    this.session.set("party",party);
  }

  updateEmail(email: String){
    this.session.set("email",email);
  }

  updateEmailThenValidateAndMove(email: String){
    this.updateEmail(email);
    this.validateAndMove();
  }

  goBack(){
    this.location.back();
  }

  validateValue(value: number){
    return value;
  }

}