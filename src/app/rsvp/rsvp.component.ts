import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {SessionStorageService, SessionStorage } from 'angular-web-storage';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-rsvp',
  templateUrl: './rsvp.component.html',
  styleUrls: ['./rsvp.component.css']
})
export class RSVPComponent implements OnInit {
  attending: any;

  constructor(public firestore: AngularFirestore, private router: Router, public session: SessionStorageService) { }

  ngOnInit(): void {
  }

  validateAndMove(){
    if(this.session.get("adults") != null && this.session.get("partyName") != null && this.session.get("children") != null && this.session.get("email")){
	  if(this.session.get("attending") == "y"){
		this.firestore.collection("RSVP-Yes-2021").add({
			adult: this.session.get("adults"),
			children: this.session.get("children"),
			partyName: this.session.get("partyName"),
			email: this.session.get("email")
		});
	  }else{
		this.firestore.collection("RSVP-No-2021").add({
			adult: this.session.get("adults"),
			children: this.session.get("children"),
			partyName: this.session.get("partyName"),
			email: this.session.get("email")
		});
	  }
	  
      this.router.navigate(['/home']);
    }else{
	  alert("Some information is missing!");
	  console.log(this.session.get('adults'), this.session.get('children'), this.session.get('partyName'), this.session.get('email'));
    }
  }

  updateAdults(adults: number) {
	let adultInput = document.getElementById('adults');
	if (!isNaN(adults)) { // isNaN on false represents a valid numerical value
		if (adultInput.className.includes('is-invalid')) {
			adultInput.classList.remove('is-invalid');
			adultInput.classList.add('is-valid');
		}
		else if (!adultInput.className.includes('is-valid')) {
			adultInput.classList.add('is-valid');
		}
		this.session.set("adults",adults);
	}
	else {
		if (adultInput.className.includes('is-valid')) {
			adultInput.classList.remove('is-valid');
			adultInput.classList.add('is-invalid');
		}
		else if (!adultInput.className.includes('is-invalid')) {
			adultInput.classList.add('is-invalid');
		}
	}
  }

  updateChildren(children: number){
	let childrenInput = document.getElementById('children');
	if (!isNaN(children)) { // isNaN on false represents a valid numerical value
		if (childrenInput.className.includes('is-invalid')) {
			childrenInput.classList.remove('is-invalid');
			childrenInput.classList.add('is-valid');
		}
		else if (!childrenInput.className.includes('is-valid')) {
			childrenInput.classList.add('is-valid');
		}
		this.session.set("children",children);
	}
	else {
		if (childrenInput.className.includes('is-valid')) {
			childrenInput.classList.remove('is-valid');
			childrenInput.classList.add('is-invalid');
		}
		else if (!childrenInput.className.includes('is-invalid')) {
			childrenInput.classList.add('is-invalid');
		}
	}
  }

  updatePartyName(partyName: string){
	let partyNameInput = document.getElementById('partyName');
	if (!partyNameInput.className.includes('is-valid')) { // no validation
		partyNameInput.classList.add('is-valid');
	}
	this.session.set("partyName",partyName);
  }

  updateAttending(attending: string){
	this.attending = attending;
	this.session.set("attending", attending);
	console.log(this.attending);
  }

  updateEmail(email: string){
	let emailInput = document.getElementById('email');
	let regexObj = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; // email regex RFC 5322: simplified (doesn't handle ip addresses)
	console.log('email regex', regexObj.test(email));
	if (regexObj.test(email)) {
		if (emailInput.className.includes('is-invalid')) {
			emailInput.classList.remove('is-invalid');
			emailInput.classList.add('is-valid');
		}
		else if (!emailInput.className.includes('is-valid')) {
			emailInput.classList.add('is-valid');
		}
		this.session.set("email",email);
	}
	else {
		if (emailInput.className.includes('is-valid')) {
			emailInput.classList.remove('is-valid');
			emailInput.classList.add('is-invalid');
		}
		else if (!emailInput.className.includes('is-invalid')) {
			emailInput.classList.add('is-invalid');
		}
	}
  }

  updateEmailAndMove(email: string){
    this.updateEmail(email);
    this.validateAndMove();
  }

}
