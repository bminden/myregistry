import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {SessionStorageService, SessionStorage } from 'angular-web-storage';
import { Location } from '@angular/common';

@Component({
  selector: 'app-thank',
  templateUrl: './thank.component.html',
  styleUrls: ['./thank.component.css']
})
export class ThankComponent implements OnInit {
  party: any;
  email: any;

  constructor(public location: Location, private router: Router, public session: SessionStorageService) { }

  ngOnInit(): void {
    this.party = this.session.get("party");
    this.email = this.session.get("email");
  }

  goBack(){
    this.router.navigateByUrl("/home");
  }
}
