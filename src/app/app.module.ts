import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PaymentComponent } from './payment/payment.component';
import { ThankComponent } from './thank/thank.component';
import { AngularWebStorageModule } from 'angular-web-storage';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { HomeComponent } from './home/home.component';
import { RSVPComponent } from './rsvp/rsvp.component';
import { SecretComponent } from './secret/secret.component';
import { TravelComponent } from './travel/travel.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'registry', component: HomepageComponent},
  {path: 'payment', component: PaymentComponent},
  {path: 'thanks', component: ThankComponent},
  {path: 'confirm', component: ConfirmationComponent},
  {path: 'RSVP', component: RSVPComponent},
  {path: 'travel', component: TravelComponent},
  {path: 'BlaineAndSanya', component: SecretComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomepageComponent,
    PaymentComponent,
    ThankComponent,
    ConfirmationComponent,
    HomeComponent,
    RSVPComponent,
    SecretComponent,
    TravelComponent
  ],
  imports: [
    BrowserModule,
    AngularWebStorageModule,
	AngularFireModule.initializeApp(environment.firebase),
	NgbModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {}