// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCE5_Y2qXU18ee5T6hZ1jK-Cmj0K0NgyYE",
    authDomain: "registry-bands.firebaseapp.com",
    databaseURL: "https://registry-bands.firebaseio.com",
    projectId: "registry-bands",
    storageBucket: "registry-bands.appspot.com",
    messagingSenderId: "443865264048",
    appId: "1:443865264048:web:b5714d52a0ae1fb864f075",
    measurementId: "G-L2RLKHEF9D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
