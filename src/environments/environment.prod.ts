export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCE5_Y2qXU18ee5T6hZ1jK-Cmj0K0NgyYE",
    authDomain: "registry-bands.firebaseapp.com",
    databaseURL: "https://registry-bands.firebaseio.com",
    projectId: "registry-bands",
    storageBucket: "registry-bands.appspot.com",
    messagingSenderId: "443865264048",
    appId: "1:443865264048:web:b5714d52a0ae1fb864f075",
    measurementId: "G-L2RLKHEF9D"
  }
};
